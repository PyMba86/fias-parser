package com.github.andreevdm.fias.parser;

import com.github.andreevdm.fias.model.AddressObject;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.Map;
import java.util.Set;


public class AddressObjectHandler extends FiasHandler<AddressObject> {

    private final int levelFilter;
    private final Map<String, AddressObject> addressObjects;
    private final Set<String> addressObjectsFilter;

    public AddressObjectHandler(Callback<AddressObject> callback, int levelFilter) {
        this(callback, levelFilter, null, null);
    }

    public AddressObjectHandler(Callback<AddressObject> callback, int levelFilter, Map<String, AddressObject> addressObjects,  Set<String> addressObjectsFilter) {
        super("Object", callback);
        this.levelFilter = levelFilter;
        this.addressObjects = addressObjects;
        this.addressObjectsFilter = addressObjectsFilter;
    }

    @Override
    protected AddressObject onElement() {
        int level = getInt("AOLEVEL");
        if (levelFilter > 0 && levelFilter != level) {
            return null;
        }
        String guid = get("AOGUID");
        String parentguid = get("PARENTGUID");
        if (addressObjectsFilter != null && !addressObjectsFilter.contains(parentguid)) {
            return null;
        }
        AddressObject addressObject = new AddressObject(guid, level);

        if (addressObjects != null) {
            addressObject.setParentAddressObject(addressObjects.get(parentguid));
        }

        addressObject.setFormalName(get("FORMALNAME"));
        addressObject.setShortName(get("SHORTNAME"));
        return addressObject;
    }


}
