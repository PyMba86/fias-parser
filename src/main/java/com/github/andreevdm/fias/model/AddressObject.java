package com.github.andreevdm.fias.model;


public class AddressObject {
    private final String guid;
    private final int level;

    private String formalName;
    private String shortName;

    public AddressObject getParentAddressObject() {
        return parentAddressObject;
    }

    public void setParentAddressObject(AddressObject parentAddressObject) {
        this.parentAddressObject = parentAddressObject;
    }

    private AddressObject parentAddressObject;


    public AddressObject(String guid, int level) {
        this.guid = guid;
        this.level = level;
    }

    public String getType(){
        return shortName;
    }

    public String getGuid() {
        return guid;
    }


    public String getFormalName() {
        return formalName;
    }

    public void setFormalName(String formalName) {
        this.formalName = formalName;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getName(){
        return formalName;
    }

    @Override
    public String toString() {
        return shortName + " " + getName();
    }

    public int getLevel() {
        return level;
    }
}
