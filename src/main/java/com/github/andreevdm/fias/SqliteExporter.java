package com.github.andreevdm.fias;

import com.github.andreevdm.fias.model.AddressObject;
import com.github.andreevdm.fias.parser.AddressObjectHandler;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.xml.parsers.*;
import java.io.File;
import java.io.FileInputStream;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;


public class SqliteExporter {

    private static final Logger log = LogManager.getLogger();

    private Map<String,Integer> cityMapper = new HashMap<>();


    private void addCity(int code, String value) {
        cityMapper.put(value,code);
    }

    public  void export(String addressObjectFile) throws Exception {

        // Получил города и поселки - 4
        final Map<String, AddressObject> addressObjects = getAddressObjects(addressObjectFile, 4);

        addCity(1,"Сургут");
        addCity(2,"Ханты-Мансийск");
        addCity(3,"Нягань");
        addCity(4,"Урай");
        addCity(5,"Советский");
        addCity(6,"Лангепас");
        addCity(7,"Лянтор");
        addCity(8,"Покачи");
        addCity(9,"Нижневартовск");
        addCity(10,"Пыть-Ях");
        addCity(11,"Нефтеюганск");
        addCity(12,"Югорск");
        addCity(13,"Белоярский");
        addCity(14,"Солнечный");
        addCity(15,"Мегион");
        addCity(16,"Когалым");
        addCity(17,"Радужный");

        // Получить улицы - 7 уровень
        final Map<String, AddressObject> addressStreetObjects
                = getAddressObjects(addressObjectFile, 7, addressObjects);

        // Сохранить адресса
        saveAddressObjects(new ArrayList<AddressObject>(addressStreetObjects.values()));
    }


    private static Map<String, AddressObject> getAddressObjects(String file, int level) throws Exception {
        final Map<String, AddressObject> addressObjects = new HashMap<String, AddressObject>();
        SAXParserFactory factory = SAXParserFactory.newInstance();
        SAXParser parser = factory.newSAXParser();
        parser.parse(new FileInputStream(file), new AddressObjectHandler(
                addressObject -> addressObjects.put(addressObject.getGuid(), addressObject),
                level
        ));
        return addressObjects;
    }

    private static Map<String, AddressObject> getAddressObjects(String file, int level, Map<String, AddressObject> parentAddressObjects) throws Exception {

        final Map<String, AddressObject> addressObjects = new HashMap<String, AddressObject>();
        SAXParserFactory factory = SAXParserFactory.newInstance();
        SAXParser parser = factory.newSAXParser();
        parser.parse(new FileInputStream(file), new AddressObjectHandler(
                addressObject -> addressObjects.put(addressObject.getGuid(), addressObject),
                level,
                parentAddressObjects,
                parentAddressObjects.keySet()
        ));
        return addressObjects;
    }

    private  void saveAddressObjects(final List<AddressObject> addressObjects) {
        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = null;
        try {
            docBuilder = docFactory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }


        Document doc = Objects.requireNonNull(docBuilder).newDocument();
        Element root= doc.createElement("hiblock");

        doc.appendChild(root);

        Element rootElement = doc.createElement("items");
        root.appendChild(rootElement);


        addressObjects.forEach((address) -> {



            Element item = doc.createElement("item");
            rootElement.appendChild(item);



            Element name = doc.createElement("uf_street_name");
            name.appendChild(doc.createTextNode(address.toString()));
            item.appendChild(name);


            Integer code = cityMapper.get(address.getParentAddressObject().getFormalName());


            Element email = doc.createElement("uf_city_id");
            email.appendChild(doc.createTextNode(code.toString()));
            item.appendChild(email);


        });


        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = null;
        try {
            transformer = transformerFactory.newTransformer();
        } catch (TransformerConfigurationException e) {
            e.printStackTrace();
        }
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
        DOMSource source = new DOMSource(doc);

        /** Output to file */
         StreamResult result = new StreamResult(new File("/home/pymba86/Документы/projects/pymba86/fastinfoset-util/format/fi/xml/result.xml"));

        /** Output to console */
      //  StreamResult result = new StreamResult(System.out);

        try {
            transformer.transform(source, result);
        } catch (TransformerException e) {
            e.printStackTrace();
        }
        System.out.println(result);
    }


}
